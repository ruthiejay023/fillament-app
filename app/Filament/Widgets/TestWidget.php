<?php

namespace App\Filament\Widgets;

use App\Models\Post;
use App\Models\User;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Support\Enums\IconPosition;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;

class TestWidget extends BaseWidget
{
    use InteractsWithPageFilters;
    protected function getStats(): array
    {
        // $start = $this->filters['startDate'];
        // $end = $this->filters['endDate'];
        // return [
        //     Stat::make(
        //         'New Users',
        //         User::when(
        //                 $start,
        //                 fn ($query) => $query->whereDate('created_at', '>', $start)
        //             )
        //             ->when(
        //                 $end,
        //                 fn ($query) => $query->whereDate('created_at', '<', $end)
        //             )
        //             ->count()
        //     )
        //         ->description('Newly joined users')
        //         ->descriptionIcon('heroicon-o-user-group', IconPosition::Before)
        //         ->chart([1, 3, 5, 10, 20, 15, 55, 80, 40])
        //         ->color('success'),
        //     Stat::make('New Users', Post::count())
        //         ->description('All Posts')
        //         ->descriptionIcon('heroicon-o-folder', IconPosition::After)
        //         ->chart([1, 3, 5, 10, 20, 15, 55, 80, 40])
        //         ->color('info'),
        // ];

        return [
            Stat::make('New Users', User::count())
                ->description('Newly joined users')
                ->descriptionIcon('heroicon-o-user-group', IconPosition::Before)
                ->chart([1, 3, 5, 10, 20, 15, 55, 80, 40])
                ->color('success'),
            Stat::make('New Users', Post::count())
                ->description('All Posts')
                ->descriptionIcon('heroicon-o-folder', IconPosition::After)
                ->chart([1, 3, 5, 10, 20, 15, 55, 80, 40])
                ->color('info'),
        ];
    }
}
