<?php

namespace App\Filament\Widgets;

use Filament\Widgets\ChartWidget;

class TestPieChartWidget extends ChartWidget
{
    protected static ?string $heading = 'Test Pie Chart';

    protected function getData(): array
    {
        return [
            'datasets' => [
                [
                    'label' => 'Blog posts created',
                    'data' => [600, 365, 450],
                    'backgroundColor' => [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],

                ],
            ],
            'labels' => ["A", "B", "C"],
        ];
    }

    protected function getType(): string
    {
        return 'doughnut';
    }
}
